# Peltosäätutka

Javascript härpäke joka hakee säätutkan sadekertymäarvot 1, 12 ja 24 tunnin ajalta haluamiisi pisteisiin

Koordinaatit syötetään ERTS-TM35FIN muodossa. Haluamiasi paikkoja voit hakea esim kansalaisen karttapaikalta: https://asiointi.maanmittauslaitos.fi/karttapaikka/

Uudet tutkatiedot haetaan kun päivität sivun.

Paikkakokoelman jakamiseen soveltuva linkki löytyy sivun alaidasta. Peltosäätutka tallentaa paikat selaimeesi ja yksinkertaistaa sivun osoitteen. 

## Authors and acknowledgment
Perustuu Ilmatieteenlaitoksen avoimeen sääaineistoon

## License


## Project status
